﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client_New.Events
{
    public class IntegerEventArgs : EventArgs
    {

        public int Data
        {
            get;
            private set;
        }

        public IntegerEventArgs(int data)
        {
            Data = data;
        }

    }
}
