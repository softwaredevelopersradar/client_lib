﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client_New
{
    public class BytesEventArgs : EventArgs
    {
        public byte[] Data { get; private set; }

        public BytesEventArgs(byte[] data)
        {
            Data = data;
        }

    }

}
