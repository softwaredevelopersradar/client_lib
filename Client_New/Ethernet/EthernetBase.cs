﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client_New.Ethernet
{
    public class EthernetBase
    {

        private UdpClient udpClient;
        private Thread thrRead;

        #region Events
        public EventHandler<EventArgs> OnOpenPort;
        public EventHandler<EventArgs> OnClosePort;
        public EventHandler<BytesEventArgs> OnWriteByte; 
        public EventHandler<BytesEventArgs> OnReadBytes;
        #endregion

        public bool IsOpen
        {
            get { return udpClient != null; }
        }
            

        public EthernetBase()
        {

        }


        private int Port;
       

        public bool Connect(int port)
        {
            if (udpClient is null)
            { 
                udpClient = new UdpClient(port); 
                Port = port;
            }


            try
            {
                thrRead = new Thread(new ThreadStart(ReadData));
                thrRead.IsBackground = true;
                thrRead.Start();
                OnOpenPort?.Invoke(this, new EventArgs());
                return true;

            }
            catch
            {
                OnClosePort?.Invoke(this, new EventArgs());
                return false;

            }
        }

        // Close COM port
        public bool Disconnect()
        {
            // clear in buffer

            try
            {
                // close port
                udpClient.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                OnClosePort?.Invoke(this, new EventArgs());
                return true;
            }

            catch (System.Exception)
            {
                return false;
            }

        }

        protected bool WriteCodegram(byte[] bSend, string ip, int port)
        {
            try
            {
                udpClient.Send(bSend, bSend.Length, ip, port);
                OnWriteByte?.Invoke(this, new BytesEventArgs(bSend));
                return true;
            }
            catch
            {

                return false;
            }

        }



        public void EndThread()
        {
            try
            {
                if (thrRead != null)
                    if (thrRead.IsAlive)
                        thrRead?.Abort();
            }
            catch { }
        }



        #region ReadFunction

        // Read data (byte) from port (thread)
        private void ReadData()
        {
            IPEndPoint remoteIp = new IPEndPoint(IPAddress.Any, Port);

            // while port open
            while (true)
            {
                
                try
                {
                    // read data
                    byte[] data = udpClient.Receive(ref remoteIp); // получаем данные

                    if (data != null && data.Length > 0)
                    {
                        // raise  event
                        OnReadBytes?.Invoke(this, new BytesEventArgs(data));
                        AddData(data);
                    }
                }

                catch (Exception ex)
                {
                    //Disconnect();
                }
            }
        }

        protected virtual void AddData(byte[] data)
        { }

    }
        #endregion
    
}