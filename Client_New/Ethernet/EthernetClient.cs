﻿using IPAddrees_PoYebanski;
using ProtocolOptima;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Client_New.Ethernet;

namespace Client_New
{
    public class EthernetClient : EthernetBase, IClient
    {
        public EventHandler<ByteEventArgs> OnCodegramSended;
        public EventHandler<StringEventArgs> OnCodegramRecieved;

        public EthernetClient() : base() {}

        public string LocalIP { get; set; }
        public byte LocalAddress { get; set; }
        public int LocalPort { get; set; }



        public void SendOnOff(byte socketNum, string stationIP, CommandType commandType, bool GPS_L1, bool GPS_L2, bool GPS_L3, bool GLONASS_L1, bool GLONASS_L2, bool GLONASS_L3, bool Galilleo_L1, bool Galilleo_L2, bool Galilleo_L3, bool Beidou_L1, bool Beidou_L2, bool Beidou_L3, byte FreqPC, int port)
        {
            byte[] bSend = Protocol.On_Off_Command(
                Protocol.FormPrefixByte(IPAddres.AddZerosIfNecessary(LocalIP), LocalAddress, IPAddres.AddZerosIfNecessary(stationIP), socketNum, commandType),
                GPS_L1, GPS_L2, GPS_L3, GLONASS_L1, GLONASS_L2, GLONASS_L3, Galilleo_L1, Galilleo_L2, Galilleo_L3, Beidou_L1, Beidou_L2, Beidou_L3, FreqPC);

            SendCodegram(bSend, stationIP, port);
            OnCodegramSended?.Invoke(bSend, new ByteEventArgs(socketNum));
        }

        public void SendPoll(byte socketNum, string stationIP, int port)
        {
            byte[] bSend = Protocol.Poll_State_Command(IPAddres.AddZerosIfNecessary(LocalIP), LocalAddress, IPAddres.AddZerosIfNecessary(stationIP), socketNum, ProtocolOptima.CommandType.POLL);
            SendCodegram(bSend, stationIP, port);
            OnCodegramSended?.Invoke(bSend, new ByteEventArgs(socketNum));
        }

        private void SendCodegram(byte[] bSend, string ip, int port)
        {
            WriteCodegram(bSend, ip, port);
        }

        protected override void AddData(byte[] data)
        {
            if (Convert.ToByte(data[0]) == 3 && Convert.ToByte(data[1]) == 2 && Convert.ToByte(data[2]) == 1 && Convert.ToByte(data[3]) == 0)
            {
                var decode = ProtocolOptima.Protocol.DecryptionCodegram(data);
                OnCodegramRecieved(decode, new StringEventArgs(IPAddres.DeleteZeroToIPIfNecessary(decode.localIpAddress.Trim('\0'))));
            }
        }
    }
}
