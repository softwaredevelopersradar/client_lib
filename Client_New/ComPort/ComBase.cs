﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client_New
{
    public class ComBase
    {
        private SerialPort _port;
        private Thread thrRead;

        public int CodegrammLength = 57;

        #region Events
        public EventHandler<EventArgs> OnOpenPort;
        public EventHandler<EventArgs> OnClosePort;        
        public EventHandler<StringEventArgs> OnWriteString;
        public EventHandler<BytesEventArgs> OnWriteByte;
        public EventHandler<StringEventArgs> OnReadString;
        public EventHandler<BytesEventArgs> OnReadBytes;
        #endregion

        public bool IsOpen
        {
            get { return _port.IsOpen; }
        }
            


        public ComBase()
        {


        }

        // Open COM port
        public bool OpenPort(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits, ClientType clientType)
        {
            // Open COM port

            if (_port == null)
                _port = new SerialPort();

            // if port is open
            if (_port.IsOpen)

                // close it
                ClosePort();

            // try to open 
            try
            {
                // set parameters of port
                _port.PortName = portName;
                _port.BaudRate = baudRate;

                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;
                
                


                _port.ReceivedBytesThreshold = 1;

                // open it
                _port.Open();

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    if (clientType == ClientType.Radio)
                        thrRead = new Thread(new ThreadStart(ReadRadioData));
                    else thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (Exception)
                {

                }

                OnOpenPort?.Invoke(this, new EventArgs());
                return true;

            }
            catch (Exception)
            {
                OnClosePort?.Invoke(this, new EventArgs());
                return false;
            }
        }

        // Close COM port
        public bool ClosePort()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch 
            {


            }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch 
            {

            }

            try
            {
                // close port
                _port.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                OnClosePort?.Invoke(this, new EventArgs());
                return true;
            }

            catch (System.Exception)
            {
                return false;
            }

        }

        protected void SwitchedRadioFrequency(bool isFrequencyTwo)
        {
            _port.RtsEnable = isFrequencyTwo;
        }

        protected bool WriteString(string sSend)
        {
            try
            {

                _port.Write(sSend);

                OnWriteString?.Invoke(this, new StringEventArgs(sSend));
                
                return true;
            }
            catch (Exception ex)
            {
                Thread.Sleep(75);
                Console.WriteLine("False sended");
                return false;
            }

        }


        protected bool WriteCodegram(byte[] bSend)
        {
            try
            {
                _port.DiscardOutBuffer();
                //_port.DiscardInBuffer();
                _port.Write(bSend, 0, bSend.Length);
                OnWriteByte?.Invoke(this, new BytesEventArgs(bSend));


                return true;
            }
            catch
            {

                return false;
            }

        }


        protected bool WriteRadioCodegram(byte[] bSend)
        {
            try
            {
                _port.DiscardOutBuffer();
                _port.Write(bSend, 0, bSend.Length);
                OnWriteByte?.Invoke(this, new BytesEventArgs(bSend));
                
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void EndThread()
        {
            try
            {
                if(thrRead != null)
                    if (thrRead.IsAlive)
                        thrRead?.Abort();
            }
            catch { }
        }

  

        #region ReadFunction

        // Read data (byte) from port (thread)
        private void ReadData()
        {
            string readedByte = "";

            // while port open
            while (true)
            {
                try
                {
                    // read data
                    readedByte = _port.ReadLine();
                    // if bytes was read
                    if (readedByte.Length > 0)
                    {
                        try
                        {
                            // resize buffer for real size of reading bytes
                            // raise  event
                            OnReadString?.Invoke(this, new StringEventArgs(readedByte));

                            AddData(readedByte);
                        }
                        catch(Exception ex)
                        {

                        }
                    }
                }

                catch(Exception ex)
                {
                    ClosePort();
                }
            }
        }

        private void ReadRadioData()
        {
            byte[] readedByte = new byte[CodegrammLength];

            // while port open
            while (true)
            {
                try
                {
                    // read data
                    _port.Read(readedByte,0,readedByte.Length);
                    // if bytes was read
                    if (readedByte.Length > 0)
                    {
                        try
                        {
                            // resize buffer for real size of reading bytes
                            // raise  event
                            OnReadBytes?.Invoke(this, new BytesEventArgs(readedByte));

                            AddData(readedByte);
                        }
                        catch
                        {

                        }
                    }
                    else // close port
                        ClosePort();
                }

                catch
                {
                    ClosePort();
                }
            }
        }


        protected virtual void AddData(string data)
        { }

        protected virtual void AddData(byte[] data)
        { }

    }
        #endregion
    
}

