﻿using IPAddrees_PoYebanski;
using System;
using System.Text;
using System.Threading;
using Client_New.Events;

namespace Client_New
{
    public class GSMClient:ComClient
    {
        public int AmountOfTrySendCommand { get; set; } = 5;

        public string Apn { get; set; } = "internet.dos.kz";

        #region CommandSend

        private const string AT = "at\r\r"; //команда для проверки подключен ли модем (если подключен то ответ: OK)

        private const string RELOAD = "at+cfun=1,1\r\r"; //сброс всех энергозависимых настроек модема

        private const string INIT_URC = "at^scfg=\"tcp/withURCs\",\"on\"\r\r"; //устанавливает принцип работы "c URC" 

        private const string CHECK_CLIENT_STATE = "at^sici=0\r\r";


        //Команды конфигурации профиля 
        private const string INIT_CONTYPE = "at^sics=0,conType,GPRS0\r\r"; //создание профиля
        private const string INIT_INACTO = "at^sics=0,\"inactTO\",\"65535\"\r\r";
        private const string INIT_APN = "at^sics=0,apn,{0}\r\r"; //устанавливает apn


        //Команды конфигурации сокета 
        private const string INIT_SOCKET_SRVTYPE = "at^siss={0},srvType,socket\r\r";
        private const string INIT_SOСKET_SRVTYPE_NONE = "at^siss={0},srvType,none\r\r";
        private const string INIT_SOCKET_CONID = "at^siss={0},conId,0\r\r"; // задаёт профиль "{0}" сокету
        private const string INIT_SOCKET_ADDRESS = "at^siss={0},address,\"socktcp://{1}:{2}\"\r\r"; // задает ip адрес "{0}" сокету (ip адрес принимающего устройства)
        private const string INIT_SOCKET_OPEN = "at^siso={0}\r\r"; //открывает "{0}" сокет

        //Команды управления сокетом 
        private const string WRITE_SOCKET = "at^sisw={0},{1}\r\r"; //записывает сообщение на "{0}" сокет (пересылает сообщение соответствующему принемающему устройству)
        private const string READ_SOCKET = "at^sisr={0},{1}\r\r"; // считывает сообщение длинной {1} символов с "{0}" сокета
        private const string READALL_SOCKET = "at^sisr={0},1500\r\r"; // считывает все сообщения с "{0}" сокета
        private const string CLOSE_SOCKET = "at^sisc={0}\r\r"; //закрывает "{0}" сокет
        private const string CHECK_SOCKET_STATE = "at^sisi={0}\r\r";


        //Команды запроса состояния
        private const string CHECK_ALLSOCKETS_STATE = "at^siso?\r\r"; //запрашивает у модема состояние всех подключённых сокетов
        private const string CHECK_MODEM_STATE_scfg = "at^scfg=?\r\r"; //запрашивает все доступные для просмотра настройки модема
        private const string CHECK_MODEM_STATE_atv = "at&v\r\r"; //запрашивает некоторые настройки модема, в том числе настройки COM-порта
        private const string SIGNAL_QUALITY_3G = "at^smoni\r\r"; //проверяет уровень сигнала
        #endregion


        #region CommandRead

        private const string URC_ANSWER = "^SCFG:\"Tcp/WithURCs\",\"on\"\r";

        private const string ANSWER_OK = "OK\r";
        private const string ANSWER_ERROR = "ERROR\r";
        private const string ANSWER_ERROR_CME = "+CME ERROR";

        private const string ANSWER_SISW = "^SISW:";
        private const string ANSWER_SISR = "^SISR:";
        private const string ANSWER_SIS = "^SIS:";
        private const string ANSWER_SISI = "^SISI:";

        private const string ANSWER_PBREADY = "+PBREADY\r";
        private const string ANSWER_SIGNAL_QUALITY_3G = "^SMONI:";


        #endregion

        #region Private Events

        private delegate void NoArgumentHandler();
        private event NoArgumentHandler OnModemReload;
        private event NoArgumentHandler OnCONTYPE;
        private event NoArgumentHandler OnURC;
        private event NoArgumentHandler OnINACTO;
        private event NoArgumentHandler OnAPN;
        private event NoArgumentHandler OnModemReloadCommandReSend;
        private event NoArgumentHandler OnSICI;

        private EventHandler<ByteEventArgs> OnSOCKET_SRVTYPE;
        private EventHandler<ByteEventArgs> OnSOCKET_CONID;
        private EventHandler<ByteEventArgs> OnSOCKET_ADDRESS;
        private EventHandler<ByteEventArgs> OnSOCKET_OPEN;
        private EventHandler<ByteEventArgs> OnSOCKET_MessageRecieved;
        private EventHandler<ByteEventArgs> OnSRVTYPEReSend;

        private EventHandler<ByteEventArgs> OnSOCKET_CLOSE;

        private EventHandler<ByteEventArgs> OnClearSRVTYPE;
        private EventHandler<ByteEventArgs> OnReInitSOCKET;

        private EventHandler<ByteEventArgs> OnSOCKET_Write;
        private EventHandler<ByteEventArgs> OnSOCKET_Read;

        private EventHandler<ByteEventArgs> OnSOCKET_Init_Failed;

        private EventHandler<ByteEventArgs> OnReadySomeDataRecived;
        private EventHandler<ByteEventArgs> OnSOCKET_Error;

        #endregion

        #region Public Events
        public delegate void NoArgumentPublicHandler();
        public NoArgumentPublicHandler OnClientReloaded;
        public NoArgumentPublicHandler OnClientRegistered;
        public EventHandler<IntegerEventArgs> OnSignalQuality;

        public EventHandler<ByteEventArgs> OnSocketInitialized;
        public EventHandler<ByteEventArgs> OnSocketConnected;
        public EventHandler<ByteEventArgs> OnReadyToSendCodegram;
        public EventHandler<ByteEventArgs> OnCodegramSended;
        public EventHandler<ByteEventArgs> OnConfirmedSendCodegram;

        public NoArgumentPublicHandler OnFindModem;
        public NoArgumentPublicHandler OnClientCheck;

        public EventHandler<ByteEventArgs> OnCloseSocket;
        public EventHandler<ByteEventArgs> OnSocketClosed;
        public EventHandler<ByteEventArgs> OnSocketClear;

        public EventHandler<ByteEventArgs> OnLostedConnection;

        public EventHandler<ByteEventArgs> OnCodegramRecieved;

        public EventHandler<ByteEventArgs> OnErrorOcured;
        public EventHandler<StringEventArgs> OnSomeErrorInitClient;
        public EventHandler<ByteEventArgs> OnSomeErrorInitSocket;
        public EventHandler<StringEventArgs> OnSomeErrorRead;
        public EventHandler<StringEventArgs> OnSomeErrorWrite;
        public EventHandler<ByteEventArgs> OnErrorClose;
        public EventHandler<ByteEventArgs> OnErrorClear;

        public EventHandler<ByteEventArgs> OnSomeMessageNotSended;
        public EventHandler<ByteEventArgs> OnSomeMessageRecived;
        public EventHandler<ByteEventArgs> OnCodegramDecode;
        public EventHandler<ByteEventArgs> OnSocketChecked;

        public EventHandler<ByteEventArgs> OnSocketRecivedBufferClear;
        #endregion

        public GSMClient() : base()
        {
            OnModemReload += Client_OnModemReload;
            OnCONTYPE += Client_OnCONTYPE;
            OnURC += Client_OnURC;
            OnINACTO += Client_OnINACTO;
            OnAPN += Client_OnAPN;
            OnModemReloadCommandReSend += Client_OnModemReloadCommandReSend;
            OnSICI += GSMClient_OnSICI;

            OnSOCKET_SRVTYPE += Client_OnSOCKET_SRVTYPE;
            OnSOCKET_CONID += Client_OnSOCKET_CONID;
            OnSOCKET_ADDRESS += Client_OnSOCKET_ADDRESS;
            OnSOCKET_OPEN += Client_OnSOCKET_OPEN;
            OnSOCKET_MessageRecieved += Client_OnSOCKET_MessageRecieved;
            OnSRVTYPEReSend += Client_SRVTYPEReSend;
            OnSOCKET_Init_Failed += Client_OnSOCKET_Init_Failed;

            OnSOCKET_CLOSE += Client_OnSOCKET_CLOSE;
            OnClearSRVTYPE += Client_OnClearSRVTYPE;
            OnReInitSOCKET += Client_OnReInitSOCKET;

            OnSOCKET_Write += Client_OnSOCKET_Write;
            OnSOCKET_Read += Client_OnSOCKET_Read;

            OnSOCKET_Error += Client_OnSOCKET_Error;

            OnReadySomeDataRecived += ReadySomeDataRecived;
        }

        private string CONID;
        private string ADDRESS;
        private string OPEN;
        private string SRVTYPE;
        private string CLOSE;
        private byte[] bSend;
        private string PREWRITE;
        private string INITAPN;
        private bool isCodegrammSended = false;
        private bool isErrorSendCommand = false;
        private string PreviousString = "";
        private int j = 0;
        private int k = 0;



        #region SendFunction
        public bool SendStartCommand()
        {
            return WriteString(AT);
        }

        public void InitializeClient()
        {
            k = 0;
            j = 0;
            PreviousString = "";
            isCodegrammSended = false;
            SRVTYPE = ""; CONID = ""; ADDRESS = ""; OPEN = ""; CLOSE = ""; PREWRITE = "";

            SendInitURC();
        }

        public void InitializeSocket(int socketNum, string remoteIP, int remotePort)
        {
            SRVTYPE = String.Format(INIT_SOCKET_SRVTYPE, socketNum);
            CONID = String.Format(INIT_SOCKET_CONID, socketNum);
            ADDRESS = String.Format(INIT_SOCKET_ADDRESS, socketNum, IPAddres.DeleteZeroToIPIfNecessary(remoteIP), remotePort);
            OPEN = String.Format(INIT_SOCKET_OPEN, socketNum);
            InitializeSocketSRVTYPE();
        }

        public void SendPollSignalQuality()
        {
            SendSignalQualityPoll_3G();
        }

        public void SendCloseSocket(byte socketNum)
        {
            CLOSE = String.Format(CLOSE_SOCKET, socketNum);
            CloseSocket();
        }

        public void SendOpenSocket(byte socketNum)
        {
            OPEN = String.Format(INIT_SOCKET_OPEN, socketNum);
        }


        public override void SendOnOff(byte socketNum, string stationIP, ProtocolOptima.CommandType commandType, bool GPS_L1, bool GPS_L2, bool GPS_L3, bool GLONASS_L1, bool GLONASS_L2, bool GLONASS_L3, bool Galilleo_L1, bool Galilleo_L2, bool Galilleo_L3, bool Beidou_L1, bool Beidou_L2, bool Beidou_L3, byte FreqPC, int port = 1234)
        {
            bSend = ProtocolOptima.Protocol.On_Off_Command(
                    ProtocolOptima.Protocol.FormPrefixByte(IPAddres.AddZerosIfNecessary(LocalIP), LocalAddress, IPAddres.AddZerosIfNecessary(stationIP), socketNum, commandType),
                     GPS_L1, GPS_L2, GPS_L3, GLONASS_L1, GLONASS_L2, GLONASS_L3, Galilleo_L1, Galilleo_L2, Galilleo_L3, Beidou_L1, Beidou_L2, Beidou_L3, FreqPC);
            while (PreSendCommand(socketNum, bSend.Length) == false) { }           
        }

        public override void SendPoll(byte socketNum, string stationIP, int port = 1234)
        {
            bSend = ProtocolOptima.Protocol.Poll_State_Command(IPAddres.AddZerosIfNecessary(LocalIP), LocalAddress, IPAddres.AddZerosIfNecessary(stationIP), socketNum, ProtocolOptima.CommandType.POLL);
            while (PreSendCommand(socketNum, bSend.Length) == false) { }
        }

        public void ReadCodegramm(byte socketNum)
        {
            PreReadCommand(socketNum);
        }

        public void ReadALLCodegramm(byte socketNum)
        {
            PreReadALLCommand(socketNum);
        }

        public void CheckSocketStatus(byte socketNum)
        {
            SendCheckStatus(socketNum);
        }

        public void SendClearSocket(byte socketNum)
        {
            CLearSocketSRVTYPE(socketNum);
        }

        public void CheckAllSocket()
        {
            AllSocketCHECK();
        }

        public void CheckModem_scfg()
        {
            ModemCHECK_scfg();
        }

        public void CheckModem_atv()
        {
            ModemCHECK_atv();
        }



        private bool SendCheckStatus(byte socketNum)
        {
            return WriteString(String.Format(CHECK_SOCKET_STATE, socketNum));
        }

        private bool SendRestartModem()
        {
            return WriteString(RELOAD);   
        }

        private bool SendSignalQualityPoll_3G()
        {
            return WriteString(SIGNAL_QUALITY_3G);
        }

        private bool SendInitURC()
        {
            Thread.Sleep(1000);
            return WriteString(INIT_URC);
        }

        private bool SendCheckClient()
        {
            return WriteString(CHECK_CLIENT_STATE);
        }

        private bool SendInitCONTYPE()
        {
            return WriteString(INIT_CONTYPE);
        }

        private bool SendInitINACT()
        {
            Thread.Sleep(1000);
            return WriteString(INIT_INACTO);
        }

        private bool SendInitAPN()
        {
            INITAPN = String.Format(INIT_APN, Apn);
            return WriteString(INITAPN);
        }

        private bool CloseSocket()
        {
            return WriteString(CLOSE);
        }

        private bool InitializeSocketSRVTYPE()
        {
            return WriteString(SRVTYPE);     
        }

        private bool CLearSocketSRVTYPE(int socketNum)
        {
            return WriteString(String.Format(INIT_SOСKET_SRVTYPE_NONE, socketNum));
        }

        private bool AllSocketCHECK()
        {
            return WriteString(CHECK_ALLSOCKETS_STATE);
        }

        public bool ModemCHECK_scfg()
        {
            return WriteString(CHECK_MODEM_STATE_scfg);
        }

        public bool ModemCHECK_atv()
        {
            return WriteString(CHECK_MODEM_STATE_atv);
        }

        private bool InitializeSocketCONID()
        {
            return WriteString(CONID);          
        }
        private bool InitializeSocketADDRESS()
        {
            return WriteString(ADDRESS);
        }

        private bool SocketOPEN()
        {
            return WriteString(OPEN);
        }

        private bool PreSendCommand(int NumOfSocket, int Length)
        {
            PREWRITE = String.Format(WRITE_SOCKET, NumOfSocket, Length);
            return WriteString(PREWRITE);
        }

        private bool SendCodegram()
        {
            isCodegrammSended = true;
            return WriteCodegram(bSend);
        }
        private bool PreReadCommand(int NumOfSocket)
        {
            return WriteString(String.Format(READ_SOCKET, NumOfSocket, CodegrammLength));
        }

        private bool PreReadALLCommand(int NumOfSocket)
        {
            return WriteString(String.Format(READALL_SOCKET, NumOfSocket));
        }

        #endregion

        #region ReadData

        protected override void AddData(string data)
        {
            if (data.Equals(ANSWER_PBREADY))
            { 
                OnModemReload?.Invoke(); 
            }
            else if (data.Equals(ANSWER_OK))
            {
                OKHandler();
            }
            else if (data.Equals(ANSWER_ERROR))
            {
                ERRORHandler();
            }
            else if (data.Contains(ANSWER_ERROR_CME))
            {
                ERRORHandler();
            }
            else if (data.Contains(ANSWER_SIGNAL_QUALITY_3G))
            {
                string[] vs = data.Split(',');
                SignalQualityAnswer(vs);
            }
            else if (data.Contains(ANSWER_SISW))
            {
                string[] vs = data.Split(',');
                AnswerSISWHendler(vs);
            }
            else if (data.Contains(ANSWER_SISR))
            {
                string[] vs = data.Split(',');
                AnswerSISRHandler(vs);
            }
            else if (data.Contains(ANSWER_SIS))
            {
                string[] vs = data.Split(',');
                AnswerSISHandler(vs);
            }
            else if (Convert.ToByte(data[0]) == 3 && Convert.ToByte(data[1]) == 2 && Convert.ToByte(data[2]) == 1 && Convert.ToByte(data[3]) == 0)
            {
                CodegramHandler(data);
            }
            else if(data.Contains("^SISI"))
            {
                string[] vs = data.Split(',');
                AnswerSISIHandler(vs);
              
            }

            if (data.Contains("at") || data.Equals(ANSWER_PBREADY))//!data.Equals("") && data != String.Empty && !data.Equals("\r"))
                PreviousString = data;

        }

        private void SignalQualityAnswer(string[] data)
        {
            if (data.Length > 4)
            {
                int SignalQuality;
                if (data[0].Contains("3G"))
                {
                    if (int.TryParse(data[4].Trim(','), out SignalQuality))
                    {
                        SignalQuality = CalculateSignalQuality(SignalQuality);
                        OnSignalQuality?.Invoke(SignalType._3G, new IntegerEventArgs(SignalQuality));
                    }
                    else OnSignalQuality?.Invoke(SignalType._3G, new IntegerEventArgs(-1));
                }
                else if (data[0].Contains("2G"))
                {
                    if (int.TryParse(data[2].Trim(','), out SignalQuality))
                    {
                        SignalQuality = CalculateSignalQuality(SignalQuality);
                        OnSignalQuality?.Invoke(SignalType._2G, new IntegerEventArgs(SignalQuality));
                    }
                    else OnSignalQuality?.Invoke(SignalType._2G, new IntegerEventArgs(-1));
                }
            }
            else OnSignalQuality?.Invoke(SignalType.NoSignal, new IntegerEventArgs(-1));
        }

        private int CalculateSignalQuality(int signalQuality)
        {
            signalQuality = (int)(Math.Round(((-10.0 * (signalQuality + 47.0)) / (-63.0)) + 10.0));
            if (signalQuality >= 10)
                return 10;
            if (signalQuality <= 0)
                return 0; 
            return signalQuality;
        }

        private void OKHandler()
        {
            if (!PreviousString.Contains("srvType,none"))
                isErrorSendCommand = false;
            switch (PreviousString)
            {
                case AT:
                    OnFindModem();               
                    break;
                case INIT_URC:
                    OnURC();
                    break;
                case CHECK_CLIENT_STATE:
                    OnAPN();
                    break;
                case INIT_INACTO:
                    OnINACTO();
                    break;
                case INIT_CONTYPE:
                    OnCONTYPE();
                    break;
                default:
                    break;
            }

            if(PreviousString.Equals(INITAPN))
                OnAPN();
            if (PreviousString.Equals(SRVTYPE))
                OnSOCKET_SRVTYPE?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
            else if (PreviousString.Equals(CONID))
                OnSOCKET_CONID?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
            else if (PreviousString.Equals(ADDRESS))
                OnSOCKET_ADDRESS?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
            else if (PreviousString.Equals(OPEN))
                OnSOCKET_OPEN?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
            else if (PreviousString.Equals(CLOSE))
            {
                OnSocketClosed(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                OnSOCKET_CLOSE(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
            }
            else if (PreviousString.Contains("srvType,none") && isErrorSendCommand == true)
                OnReInitSOCKET?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
            else if (PreviousString.Contains("srvType,none") && isErrorSendCommand == false)
                OnSocketClear?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
            else if (PreviousString.Contains("at^sisr") && !PreviousString.Contains("1500"))
                OnCodegramDecode?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
            else if (PreviousString.Contains("sisi"))
                OnSocketChecked?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
            else if (PreviousString.Contains("at^sisr") && PreviousString.Contains("1500"))
                OnSocketRecivedBufferClear?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));

            j = 0;
            k = 0;

           
        }

        private void ERRORHandler()
        {
            if (j < AmountOfTrySendCommand)
            {
                switch (PreviousString)
                {
                    case AT:
                        OnFindModem();
                        break;
                    case RELOAD:
                        OnModemReloadCommandReSend();
                        break;
                    case INIT_URC:
                        OnModemReload();
                        break;
                    case CHECK_CLIENT_STATE:
                        OnSICI();
                        break;
                    case INIT_CONTYPE:
                        OnAPN();
                        break;
                    case INIT_INACTO:
                        OnURC();
                        break;
                    default:
                        break;
                }
                isErrorSendCommand = false;

                if (PreviousString.Equals(INITAPN))
                    OnCONTYPE();

                if (PreviousString.Equals(SRVTYPE))
                    OnSRVTYPEReSend?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                else if (PreviousString.Equals(CONID))
                    OnSOCKET_SRVTYPE?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                else if (PreviousString.Equals(ADDRESS))
                    OnSOCKET_CONID?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                else if (PreviousString.Equals(OPEN))
                    OnSOCKET_ADDRESS?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                else if (PreviousString.Equals(CLOSE))
                {
                    OnErrorClose?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                    OnSOCKET_CLOSE?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                    j = 0;
                }
                else if (PreviousString.Contains("srvType,none"))
                {
                    OnErrorClear?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                    j = 0;
                }
                else if (PreviousString.Contains("at^sisr") && PreviousString.Contains("1500"))
                { OnSocketRecivedBufferClear?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1)))); j = 0; }
            }
            else
            {
                switch (PreviousString)
                {
                    case RELOAD:
                    case INIT_URC:
                    case INIT_CONTYPE:
                    case INIT_INACTO:
                        OnSOCKET_Init_Failed(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                        break;
                    default:
                        break;
                }

                if (PreviousString.Equals(INITAPN))
                    OnSOCKET_Init_Failed(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));


                if (PreviousString.Equals(SRVTYPE) || PreviousString.Equals(CONID) || PreviousString.Equals(ADDRESS) || PreviousString.Equals(OPEN))
                {
                    if (k < 1)
                    {
                        k++;
                        OnClearSRVTYPE?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                    }
                    else
                    {
                        k = 0;
                        OnSomeErrorInitSocket?.Invoke(new StringEventArgs("Socket " + PreviousString.Substring(8, 1) + " initialization error"), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                    }
                }
                isErrorSendCommand = true;
            }

            if (j < AmountOfTrySendCodegram)
            {
                if (PreviousString.Contains("at^sisw"))
                    OnSOCKET_Write?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                else
                if (PreviousString.Contains("at^sisr"))
                    OnSOCKET_Read?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
                isErrorSendCommand = false;
            }
            else
            {
                if (PreviousString.Contains("at^sisw"))
                    OnSomeErrorWrite?.Invoke(new object(), new StringEventArgs("Write error on socket " + PreviousString.Substring(8, 1)));
                else if (PreviousString.Contains("at^sisr"))
                    OnSomeErrorRead?.Invoke(new object(), new StringEventArgs("Read error on socket " + PreviousString.Substring(8, 1)));
                isErrorSendCommand = true;
            }

            j++;
        }

        private void AnswerSISWHendler(string[] vs)
        {
           
            if (vs.Length > 2)
            {
                isCodegrammSended = true;
                OnReadySomeDataRecived?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(vs[0].Substring(7, 1))));
                if (Convert.ToByte(vs[2]) > 0)
                {
                   
                    // OnSomeMessageNotSended.Invoke(new object(), new ByteEventArgs(Convert.ToByte(vs[1])));
                    // OnSOCKET_Error(new object(), new ByteEventArgs(Convert.ToByte(vs[1])));
                }
            }
            else if (Convert.ToInt32(vs[1]) == 1 && isCodegrammSended == false && vs.Length <= 2)
            {
                OnSocketConnected?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(vs[0].Substring(7, 1))));
                return;
            }
            else if (Convert.ToInt32(vs[1]) == 1 && isCodegrammSended == true && vs.Length <= 2)
            {
                isCodegrammSended = false;
                OnConfirmedSendCodegram?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(vs[0].Substring(7, 1))));
            }
            else if (Convert.ToInt32(vs[1]) == 2)
            {
                OnErrorOcured?.Invoke("socket " + vs[0].Substring(7, 1) + " close for send and read data", new ByteEventArgs(Convert.ToByte(vs[0].Substring(7, 1))));
            }

        }

        private void AnswerSISRHandler(string[] vs)
        {
            if (vs.Length == 2 && Convert.ToInt32(vs[1]) == 1)
            {
                OnSomeMessageRecived?.Invoke(new object(), new ByteEventArgs(Convert.ToByte(vs[0].Substring(7, 1))));
            }
            if (vs.Length == 3)
            {
             
            }
        }

        private void AnswerSISHandler(string[] vs)
        {
            string socketNum = vs[0].Substring(6, 1);
            byte socketNumByte = Convert.ToByte(socketNum);
            OnErrorOcured?.Invoke("Error with socket number " + socketNum.ToString(), new ByteEventArgs(socketNumByte));
            //OnSOCKET_Error(new object(), new ByteEventArgs(socketNumByte));
        }

        private void CodegramHandler(string data)
        {
            byte[] bb = new byte[CodegrammLength];

            bb = Encoding.ASCII.GetBytes(data);
            OnSOCKET_MessageRecieved?.Invoke(ProtocolOptima.Protocol.DecryptionCodegram(bb), new ByteEventArgs(Convert.ToByte(PreviousString.Substring(8, 1))));
        }

        private void AnswerSISIHandler(string[] vs)
        {
            string socketNum = vs[0].Substring(7, 1);
            byte socketNumByte = Convert.ToByte(socketNum);

            if(Convert.ToInt32(vs[5].Trim('\r')) > 0)
            {
                OnSomeMessageNotSended?.Invoke(new object(), new ByteEventArgs(socketNumByte));                
            }
            else if (Convert.ToInt32(vs[1]) == 5 && Convert.ToInt32(vs[1]) == 6 && Convert.ToInt32(vs[1]) == 2)
            {
                OnErrorOcured?.Invoke("Error with socket number " + socketNum.ToString(), new ByteEventArgs(socketNumByte));
            }
        }

        #endregion

        #region Events handler

        private void Client_OnSOCKET_MessageRecieved(object sender, ByteEventArgs e)
        {
            OnCodegramRecieved.Invoke(sender, e);
        }
        private void Client_OnSOCKET_CLOSE(object sender, ByteEventArgs e)
        {
            CLearSocketSRVTYPE(e.Data);
        }

        private void ReadySomeDataRecived(object sender, ByteEventArgs e)
        {
            SendCodegram();
            OnCodegramSended?.Invoke(sender, e);
        }
        private void Client_OnSOCKET_OPEN(object sender, ByteEventArgs e)
        {
            OnSocketInitialized?.Invoke(sender, e);
        }

        private void Client_OnSOCKET_Error(object sender, ByteEventArgs e)
        {
            SendCloseSocket(e.Data);
        }


        private void Client_OnClearSRVTYPE(object sender, ByteEventArgs e)
        {
            CLearSocketSRVTYPE(e.Data);
        }

        private void Client_OnReInitSOCKET(object sender, ByteEventArgs e)
        {
            InitializeSocketSRVTYPE();
        }

        private void Client_OnClearSRVTYPEReSend(object sender, ByteEventArgs e)
        {
            CLearSocketSRVTYPE(e.Data);
        }

        private void Client_OnSOCKET_Read(object sender, ByteEventArgs e)
        {
            PreReadCommand(e.Data);
        }

        private void Client_OnSOCKET_Write(object sender, ByteEventArgs e)
        {
            PreSendCommand(e.Data, bSend.Length);
        }

       

        private void Client_OnSOCKET_Init_Failed(object sender, ByteEventArgs e)
        {
            InitializeSocketSRVTYPE();
        }

        private void SomeDataRecived(object sender, ByteEventArgs e)
        {
            PreReadCommand(e.Data);
        }

      

        private void Client_OnSOCKET_ADDRESS(object sender, ByteEventArgs e)
        {
            SocketOPEN();
        }

        private void Client_SRVTYPEReSend(object sender, ByteEventArgs e)
        {
            InitializeSocketSRVTYPE();
        }

        private void Client_OnSOCKET_CONID(object sender, ByteEventArgs e)
        {
            InitializeSocketADDRESS();
        }

        private void Client_OnSOCKET_SRVTYPE(object sender, ByteEventArgs e)
        {
            InitializeSocketCONID();
        }

        private void Client_OnAPN()
        {
            OnClientRegistered?.Invoke();
        }

        private void Client_OnINACTO()
        {
            SendInitCONTYPE();
        }

        private void GSMClient_OnSICI()
        {
            SendInitINACT();   
        }

        private void Client_OnURC()
        {
            SendCheckClient();
        }

        private void Client_OnCONTYPE()
        {
            SendInitAPN();
        }


        private void Client_OnModemReloadCommandReSend()
        {
            SendRestartModem();
        }

        private void Client_OnModemReload()
        {
            SendInitURC();
            OnClientReloaded?.Invoke();
        }

        #endregion
    }
}
