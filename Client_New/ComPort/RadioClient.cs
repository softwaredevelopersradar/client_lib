﻿using IPAddrees_PoYebanski;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client_New
{
    public class RadioClient : ComClient
    {
 
        public EventHandler<StringEventArgs> OnCodegramRecieved;
        public EventArgs OnPortConnected;
        public EventHandler OnIncorrectCodegram;
        public EventHandler<ByteEventArgs> OnCodegramSended;

        public RadioClient():base(){}

        public override void SendOnOff(byte socketNum, string stationIP, ProtocolOptima.CommandType commandType, bool GPS_L1, bool GPS_L2, bool GPS_L3, bool GLONASS_L1, bool GLONASS_L2, bool GLONASS_L3, bool Galilleo_L1, bool Galilleo_L2, bool Galilleo_L3, bool Beidou_L1, bool Beidou_L2, bool Beidou_L3, byte FreqPC, int port = 1234)
        {
            byte[] bSend = ProtocolOptima.Protocol.On_Off_Command(
                    ProtocolOptima.Protocol.FormPrefixByte(IPAddres.AddZerosIfNecessary(LocalIP), LocalAddress, IPAddres.AddZerosIfNecessary(stationIP), socketNum, commandType),
                     GPS_L1, GPS_L2, GPS_L3, GLONASS_L1, GLONASS_L2, GLONASS_L3, Galilleo_L1, Galilleo_L2, Galilleo_L3, Beidou_L1, Beidou_L2, Beidou_L3, FreqPC);

            SendCodegram(bSend);
            OnCodegramSended?.Invoke(bSend, new ByteEventArgs(socketNum));
        }

        public override void SendPoll(byte socketNum, string stationIP, int port=1234)
        {
            byte[] bSend = ProtocolOptima.Protocol.Poll_State_Command(IPAddres.AddZerosIfNecessary(LocalIP), LocalAddress, IPAddres.AddZerosIfNecessary(stationIP), socketNum, ProtocolOptima.CommandType.POLL);
            SendCodegram(bSend);
            OnCodegramSended?.Invoke(bSend, new ByteEventArgs(socketNum));
        }

        public void SwitchEadioFrequency(bool isF2Frequency)
        {
            SwitchedRadioFrequency(isF2Frequency);
        }

        private bool SendCodegram(byte[] bSend)
        {
            return WriteRadioCodegram(bSend);
        }

        protected override void AddData(byte[] data)
        {
            try
            {
                byte[] d = new byte[CodegrammLength];
                if (data[0] == 2 && data[1] == 1 && data[2] == 0)
                {
                    Array.Copy(data, 0, d, 1, data.Length - 1);
                    d[0] = 3;
                    ProtocolOptima.Model.StationCodegram protocol = ProtocolOptima.Protocol.DecryptionCodegram(d);
                    OnCodegramRecieved?.Invoke(protocol, new StringEventArgs(IPAddres.DeleteZeroToIPIfNecessary(protocol.localIpAddress.Trim('\0'))));
                }
                else if (data[0] == 3 && data[1] == 1 && data[2] == 0)
                {
                    Array.Copy(data, 1, d, 2, data.Length - 2);
                    d[0] = 3;
                    d[1] = 2;
                    ProtocolOptima.Model.StationCodegram protocol = ProtocolOptima.Protocol.DecryptionCodegram(d);
                    OnCodegramRecieved?.Invoke(protocol, new StringEventArgs(IPAddres.DeleteZeroToIPIfNecessary(protocol.localIpAddress.Trim('\0'))));
                }
                else if (data[0] == 3 && data[1] == 2 && data[2] == 1 && data[3] == 0)
                {
                    ProtocolOptima.Model.StationCodegram protocol = ProtocolOptima.Protocol.DecryptionCodegram(data);
                    OnCodegramRecieved?.Invoke(protocol, new StringEventArgs(IPAddres.DeleteZeroToIPIfNecessary(protocol.localIpAddress.Trim('\0'))));
                }
                else
                {
                    OnIncorrectCodegram?.Invoke(this, EventArgs.Empty);
                }
            }
            catch (Exception ex)
            {
                OnIncorrectCodegram?.Invoke(this, EventArgs.Empty);
            }

        }
    }
}
