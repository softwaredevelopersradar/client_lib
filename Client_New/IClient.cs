﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client_New
{
    public interface IClient
    {
         string LocalIP { get; set; }
         byte LocalAddress { get; set; } 
         int LocalPort { get; set; } 

        void SendOnOff(byte socketNum, string stationIP, ProtocolOptima.CommandType commandType, bool GPS_L1, bool GPS_L2, bool GPS_L3, bool GLONASS_L1, bool GLONASS_L2, bool GLONASS_L3, bool Galilleo_L1, bool Galilleo_L2, bool Galilleo_L3, bool Beidou_L1, bool Beidou_L2, bool Beidou_L3, byte FreqPC, int port = 1234);

        void SendPoll(byte socketNum, string stationIP, int port = 1234);
    }
}
