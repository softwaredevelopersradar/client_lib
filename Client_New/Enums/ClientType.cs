﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client_New
{
    public enum ClientType
    {
        GSM = 0,
        Radio = 1,
        Ethernet = 2
    }

    public enum SignalType
    {
        [Description("3G")]
        _3G = 0,
        [Description("2G")]
        _2G = 1,
        [Description("No signal")]
        NoSignal = 2
    }
}
