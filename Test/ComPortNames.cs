﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class ComPortNames : ObservableCollection<string>
    {
        public ComPortNames()
        {
            UpdatePort();
        }

        public void UpdatePort()
        {
            List<string> portnames = SerialPort.GetPortNames().ToList();
            if (portnames.Count == 0)
            {
                portnames.Add("COM1");
                portnames.Add("COM2");
            }

            if (this.Equals(portnames))
                return;
            var except = portnames.Except(this).ToList();
            foreach (var name in except)
                Add(name);
            except = this.Except(portnames).ToList();
            foreach (var name in except)
                Remove(name);
        }
    }
}
