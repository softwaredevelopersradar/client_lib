﻿using Client_New;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Client_New.GSMClient client = new Client_New.GSMClient();
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void conect_Click(object sender, RoutedEventArgs e)
        {

            client.OpenPort("COM49", 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One, ClientType.GSM);
        }

        private void CC(object sender, byte e)
        {
            
        }

        private void C(object sender, byte e)
        {
           
        }

        private void Write(object sender, ByteEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                log.Text += e + "\n";
            });
        }

        private void ReadData(object sender, string e)
        {
            Dispatcher.Invoke(() =>
            {
                log.Text += e +"\n";
            });
        }

        private void send_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void send_CONTYPE_Click(object sender, RoutedEventArgs e)
        {
            client.InitializeSocket(1, "46.56.149.161", 1234);
        }

        private void send_INACT_Click(object sender, RoutedEventArgs e)
        {
            client.SendOnOff(1, "46.56.149.161", ProtocolOptima.CommandType.ON, true, true, true, true, true, true, true, true, true, true, true, true, 1);
        }

        private void send_URC_Click(object sender, RoutedEventArgs e)
        {
            client.InitializeClient();
        }

        private void send_INACT_Copy_Click(object sender, RoutedEventArgs e)
        {
            client.SendOnOff(1, "46.56.149.161", ProtocolOptima.CommandType.OFF, true, true, true, true, true, true, true, true, true, true, true, true, 1);
        }
    }
}
